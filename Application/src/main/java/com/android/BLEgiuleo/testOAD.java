package com.android.BLEgiuleo;

import android.app.Activity;;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import java.lang.*;
import android.util.Log;
import android.content.DialogInterface;

import android.view.View;
import android.widget.ImageView;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ProgressBar;
import android.app.AlertDialog;


/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class testOAD extends Activity {

    private final static String TAG = testOAD.class.getSimpleName();
    /* User interface */
    private TextView log_textView;
    private TextView total_size_textView;
    private TextView image_type_textView;
    private TextView start_address_textView;
    private TextView image_version_textView;
    private TextView upload_speed_textView;
    private Button upload_button;
    private Button load_button;
    private ScrollView scrollView_log;
    private ProgressBar download_progressBar;
    private TextView progress_textView;
    private static final int READ_REQUEST_CODE = 42;
    /* Bluetooth */
    private String mDeviceAddress;
    private boolean mConnected;
    private OverAirDownload oad;
    private BluetoothLeService mBluetoothLeService;


    public void performFileSearch() {

        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        // Filter to show only images, using the image MIME data type.
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        intent.setType("application/octet-stream");

        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        // The ACTION_OPEN_DOCUMENT intent was sent with the request code
        // READ_REQUEST_CODE. If the request code seen here doesn't match, it's the
        // response to some other intent, and the code below shouldn't run at all.

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.
            if (resultData != null) {
                boolean isOK = oad.parseMetadata(resultData.getData());
                if (isOK) {
                    upload_button.setEnabled(true);
                }
                else {
                    resetFirmwareInfo();
                    upload_button.setEnabled(false);
                }
            }
        }
    }

    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            /* pass BLE service to the OAD */
            oad.setOADbluetoothLeService(mBluetoothLeService);

            if (!mConnected) {
                updateLogTextView("Not Connected .... connecting");
                if (!mBluetoothLeService.initialize()) {
                    Log.e(TAG, "Unable to initialize Bluetooth");
                    finish();
                }
                // Automatically connects to the device upon successful start-up initialization.
                mBluetoothLeService.connect(mDeviceAddress);
            }
            else updateLogTextView("Already Connected");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateLogTextView("CONNECTED");
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateLogTextView("DISCONNECTED");
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                String chara = intent.getStringExtra(BluetoothLeService.EXTRA_CHARACTERISTIC_CHANGED);
                byte[] data = intent.getByteArrayExtra(BluetoothLeService.EXTRA_DATA);
                oad.processData(chara, data);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.oad_downloader);
        log_textView = findViewById(R.id.log_textView);
        image_type_textView = findViewById(R.id.image_type_textView);
        start_address_textView = findViewById(R.id.start_address_textView);
        image_version_textView = findViewById(R.id.image_version_textView);
        total_size_textView = findViewById(R.id.total_size_textView);
        upload_speed_textView = findViewById(R.id.upload_speed_textView);
        upload_button = findViewById(R.id.upload_button);
        load_button = findViewById(R.id.load_button);
        scrollView_log = findViewById(R.id.scrollView_log);
        download_progressBar = findViewById(R.id.download_progressBar);
        progress_textView = findViewById(R.id.progress_textView);

        getActionBar().setTitle("Over the Air Download");
        log_textView.setText("");
        upload_button.setEnabled(false);

        final Intent intent = getIntent();
        mConnected = intent.getBooleanExtra(DeviceControlActivity.EXTRAS_DEVICE_CONNECTION_STATE, false);
        mDeviceAddress = intent.getStringExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS);

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        /* create OAD object */
        oad = new OverAirDownload(testOAD.this);

        /* OAD events */
        oad.setListener( new OverAirDownload.OADeventListener() {
            public void onDataLog(String text) {
                updateLogTextView(text);
            }

            public void onProgress(int codeMessage, int message) {
                if (codeMessage == oad.OAD_PROGRESS) {
                    updateProgressView(message);
                }
                else if (codeMessage == oad.OAD_RESULT) {
                    showAlertResult(message == 1);
                }
                else if (codeMessage == oad.OAD_SPEED) {
                    upload_speed_textView.setText(message + " byte/s");
                }
                else {
                    updateLogTextView("ERROR: code message not handled");
                }
            }

            public void onFirmwarerRead(String version, int filesize, int address, int type) {
                image_version_textView.setText( "v" + version );
                total_size_textView.setText( filesize + " byte" );
                start_address_textView.setText( "0x" + Integer.toString( address, 16 ));
                image_type_textView.setText( Enumerations.ImageType.get(type) );
            }
        });

        /* Human Interface event upload firmware */
        upload_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProgressView(0);

                if (mConnected) {
                    load_button.setEnabled(false);
                    upload_button.setEnabled(false);
                    oad.initializeCharacteristic();
                }
                else {
                    updateLogTextView("ERROR: target disconnected");
                }
            }
        });

        /* Human Interface event load file */
        load_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performFileSearch();
            }
        });
    }

    private void showAlertResult( boolean isSuccess ) {

        AlertDialog.Builder alertadd = new AlertDialog.Builder(this);
        LayoutInflater layout = LayoutInflater.from(this);
        View view = layout.inflate(R.layout.popup_result, null);
        ImageView iv = view.findViewById(R.id.result_imageView);
        TextView message = view.findViewById(R.id.alertMessage_textView);

        if (isSuccess) {
            iv.setImageResource(R.mipmap.alert_happy);
            message.setText("OK, exit now");
        }
        else {
            iv.setImageResource(R.mipmap.alert_sad);
            message.setText(R.string.device_bricked);
        }

        alertadd.setView(view);
        alertadd.setNeutralButton("Press and go back", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dlg, int sumthin) {

            }
        });

        alertadd.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    private void resetFirmwareInfo() {
        image_version_textView.setText( R.string.no_data );
        total_size_textView.setText( R.string.no_data );
        start_address_textView.setText( R.string.no_data );
        image_type_textView.setText( R.string.no_data );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    private void updateLogTextView(final String logtext) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                    log_textView.append(logtext + "\n");

                    scrollView_log.post(new Runnable() {
                        @Override
                        public void run() {
                            scrollView_log.fullScroll(View.FOCUS_DOWN);
                        }
                    });
            }
        });
    }

    private void updateProgressView(final int value) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progress_textView.setText(" " + value + "%");
                download_progressBar.setProgress(value);
            }
        });
    }
}
