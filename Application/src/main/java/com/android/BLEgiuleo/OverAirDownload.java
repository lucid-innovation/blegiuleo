package com.android.BLEgiuleo;

import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;

import org.apache.commons.lang3.ArrayUtils;

import java.io.*;
import java.util.*;


public class OverAirDownload {

    private final static String TAG = OverAirDownload.class.getSimpleName();
    Context context;
    /* General */
    private byte[] metadata = new byte[16];
    private DataInputStream dis;
    private Uri uriFirmwareFile;
    private int fileFirmwareSize;
    private int blockIndexSent = 0x01;
    private Timer timer;
    private int blockIndexSent500ms = 0;
    /* Bluetooth */
    private BluetoothLeService mBluetoothLeService;
    private BluetoothGattService oad_service;
    private BluetoothGattCharacteristic identify_char, block_char, count_char, status_char;

    public static final int OAD_RESULT = 0;
    public static final int OAD_PROGRESS = 1;
    public static final int OAD_SPEED = 2;


    /* Constructor */
    public OverAirDownload(Context ctx) {
        context = ctx;
    }

    public void setOADbluetoothLeService (BluetoothLeService bls) {
        mBluetoothLeService = bls;
    }

    /* Events */
    public interface OADeventListener {
        public void onFirmwarerRead(String version, int filesize, int address, int type);
        public void onDataLog(String text);
        public void onProgress(int codeMessage, int message);
    }

    private OADeventListener listener;

    public void setListener(OADeventListener lst) {
        this.listener = lst;
    }

    /*** OAD ***/
    public boolean parseMetadata(Uri uriFW) {
        uriFirmwareFile = uriFW;
        openStreamFile();
        metadata = readBlockFile();
        closeStreamFile();

        StringBuilder stringBuilder = new StringBuilder(metadata.length);
        for(byte item : metadata) {
            stringBuilder.append(String.format("%02X ", item));
        }
        listener.onDataLog("metadata: " + stringBuilder);

        /* size file is in blocks of 4 byte */
        int frmwareSize = convertArrayToInt(metadata, 6, 7) * 4;
        if (frmwareSize != fileFirmwareSize) {
            listener.onDataLog("ERROR: file size does not match with metadata");
            return false;
        }

        /* version in the form XX.YY */
        String stringVersion = (metadata[5] & 0xFF) + "." + (metadata[4] & 0xFF);
        /* start address is in blocks of 4 byte */
        int address = convertArrayToInt(metadata, 12, 13) * 4;
        /* image type */
        int imgType = (int)metadata[14];

        listener.onFirmwarerRead(stringVersion, fileFirmwareSize, address, imgType );

        return true;
    }

    private void openStreamFile() {
        try {
            ContentResolver cr = context.getContentResolver();
            InputStream is = cr.openInputStream(uriFirmwareFile);
            dis = new DataInputStream(is);
            fileFirmwareSize = is.available();
            listener.onDataLog("File Loaded");
        }
        catch(FileNotFoundException fnfe) {
            listener.onDataLog("ERROR: file not found");
        }
        catch(IOException fnfe) {
            listener.onDataLog("ERROR: file" + fnfe.toString());
        }
    }

    private void closeStreamFile() {
        try {
            dis.close();
            listener.onDataLog("File Closed");
        }
        catch(IOException fnfe) {
            listener.onDataLog("ERROR: file" + fnfe.toString());
        }
    }

    private byte[] readBlockFile() {
        byte[] filedata = new byte[16];

        try {
            dis.readFully(filedata);
        }
        catch(IOException fnfe) {
            listener.onDataLog("ERROR: file" + fnfe.toString());
        }

        return filedata;
    }

    public void processData (String characteristic, byte[] data)
    {
        if (characteristic.equals(GattAttributes.OAD_CHARACTERISTIC_BLOCK))
        {
            int blockIndexRequest = convertArrayToInt(data, 0, 1);

            if (blockIndexRequest == blockIndexSent) {
                listener.onDataLog("ERROR: block " + blockIndexRequest + " already sent");
            }
            else {
                if (blockIndexRequest == 0) {
                    openStreamFile();
                    listener.onDataLog("Firmware ACCEPTED from the target");
                    createTimer();
                }

                blockIndexSent = blockIndexRequest;

                byte[] blockData = readBlockFile();
                byte[] blocktoSend = ArrayUtils.addAll(data, blockData);
                mBluetoothLeService.writeCharacteristic(block_char, blocktoSend);
            }
        }
        else if (characteristic.equals(GattAttributes.OAD_CHARACTERISTIC_STATUS))
        {
            closeStreamFile();
            stopTimer();
            int stateImage = convertArrayToInt(data, 0,0);
            listener.onDataLog("State received: " + Enumerations.ImageState.get(stateImage));

            /* check if stateImage is equal to OAD_SUCCESS */
            if (Enumerations.ImageState.get(stateImage).equals("OAD_SUCCESS")) {
                listener.onProgress(OAD_RESULT, 1);
            }
            else {
                listener.onProgress(OAD_RESULT, 0);
            }
        }
        else if (characteristic.equals(GattAttributes.OAD_CHARACTERISTIC_IDENTIFY))
        {
            listener.onDataLog("Firmware REJECTED from the target, the resident firmware is:");

            if (data.length == 8) {
                String stringVersion = (data[1] & 0xFF) + "." + (data[0] & 0xFF);
                listener.onDataLog( "v" + stringVersion );
                int size = convertArrayToInt(data, 2,3) * 4;
                listener.onDataLog( Integer.toString(size)  + " byte");

                StringBuilder stringUser = new StringBuilder(4);
                for (int i = 4; i < data.length; i++) {
                    stringUser.append(String.format("%02X ", data[i]));
                }
                listener.onDataLog("User: " + stringUser);
            }
            else {
                listener.onDataLog("Error in the characteristic: " + characteristic);
            }
        }
        else {
            listener.onDataLog("Characteristic not handled: " + characteristic);
        }
    }

    public void initializeCharacteristic() {
        oad_service = mBluetoothLeService.getGattServiceByUUID(UUID.fromString(GattAttributes.OAD_SERVICE));
        identify_char = oad_service.getCharacteristic(UUID.fromString(GattAttributes.OAD_CHARACTERISTIC_IDENTIFY));
        block_char = oad_service.getCharacteristic(UUID.fromString(GattAttributes.OAD_CHARACTERISTIC_BLOCK));
        count_char = oad_service.getCharacteristic(UUID.fromString(GattAttributes.OAD_CHARACTERISTIC_COUNT));
        status_char = oad_service.getCharacteristic(UUID.fromString(GattAttributes.OAD_CHARACTERISTIC_STATUS));

        Thread loop = new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            listener.onDataLog("Initializing...");
                            mBluetoothLeService.setCharacteristicNotification(identify_char, true);
                            Thread.sleep(150);
                            mBluetoothLeService.setCharacteristicNotification(block_char, true);
                            Thread.sleep(150);
                            mBluetoothLeService.setCharacteristicNotification(status_char, true);
                            Thread.sleep(150);
                            byte[] imageCount = {1};
                            mBluetoothLeService.writeCharacteristic(count_char, imageCount);
                            Thread.sleep(150);
                            mBluetoothLeService.writeCharacteristic(identify_char, metadata);
                            Thread.sleep(150);
                            listener.onDataLog("Initialized");
                        } catch (InterruptedException ex) {
                            // Stop immediately and go home
                        }
                    }
                }
        );
        loop.start();
    }

    private void createTimer () {
        blockIndexSent500ms = 0;

        timer = new Timer();

        class periodicTimerTask extends TimerTask {
            @Override
            public void run() {
                ((Activity)context).runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        int byteSent = (blockIndexSent - blockIndexSent500ms) * 16;     // each block is 16 bytes
                        blockIndexSent500ms = blockIndexSent;
                        listener.onProgress(OAD_SPEED, byteSent * 2);

                        int lastByteIndex = ((blockIndexSent) * 16) + 16;
                        int progressPercent = (lastByteIndex * 100) / fileFirmwareSize;
                        listener.onProgress(OAD_PROGRESS, progressPercent);
                    }
                });
            }
        };

        timer.schedule(new periodicTimerTask(), 0,500);
    }

    private void stopTimer() {
        timer.cancel();
        timer.purge();
    }

    private int convertArrayToInt(byte[] data, int startIndex, int endIndex) {
        int[] arrayInt = new int[endIndex - startIndex + 1];
        int resultInt = 0;

        for (int i = startIndex; i < endIndex+1; i++) {
            arrayInt[i - startIndex] = data[i] & 0xFF;
        }

        switch (arrayInt.length) {
            case 1:
                resultInt = arrayInt[0];
                break;
            case 2:
                resultInt = arrayInt[0] | (arrayInt[1] << 8);
                break;
            case 4:
                resultInt = arrayInt[0] | (arrayInt[1] << 8) | (arrayInt[2] << 16) | (arrayInt[3] << 24);
                break;
            default:
                listener.onDataLog("ERROR: Array to Integer conversion");
                break;
        }

        return resultInt;
    }
}

