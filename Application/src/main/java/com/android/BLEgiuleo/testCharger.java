package com.android.BLEgiuleo;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;

public class testCharger extends Activity {
    GraphView graph;
    TextView powerRXview;
    TextView powerTXview;
    TextView powerLossView;
    double powerRX;
    double powerTX;
    double powerLoss;
    private LineGraphSeries<DataPoint> seriesPowerRX = new LineGraphSeries<>();
    private LineGraphSeries<DataPoint> seriesPowerTX = new LineGraphSeries<>();
    private static final int NO_INIT = 0xFFFFFFFF;
    private int timestampOrigin = NO_INIT;
    private double timestamp;
    private Timer timer;
    DecimalFormat df = new DecimalFormat("#.####");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setTitle(R.string.title_charger);
        setContentView(R.layout.charger_test);

        graph = findViewById(R.id.graph);
        powerRXview = findViewById(R.id.powerRXData);
        powerTXview = findViewById(R.id.powerTXData);
        powerLossView = findViewById(R.id.powerLossData);

        // data
        graph.addSeries(seriesPowerRX);
        graph.addSeries(seriesPowerTX);

        // customize a little bit viewport
        Viewport viewport = graph.getViewport();
        viewport.setYAxisBoundsManual(true);
        viewport.setXAxisBoundsManual(true);
        viewport.setMinY(0);
        viewport.setMaxY(2);
        viewport.setMinX(0);
        viewport.setMaxX(40);
        graph.getGridLabelRenderer().setNumHorizontalLabels(5);
        graph.getGridLabelRenderer().setNumVerticalLabels(6);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);

        seriesPowerRX.setColor(Color.BLUE);
        seriesPowerTX.setColor(Color.RED);
        seriesPowerRX.setTitle("power RX");
        seriesPowerTX.setTitle("power TX");
        graph.getLegendRenderer().setVisible(true);
        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
        // custom label formatter
        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show label x values
                    return super.formatLabel(value, isValueX) + "s";
                } else {
                    // show label for y values
                    return super.formatLabel(value, isValueX) + "W";
                }
            }
        });

        createTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        createTimer();
    }


    // Handles various events fired by the Service.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getByteArrayExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };

    private void displayData(byte[] data) {
        int rawField1, rawField2, rawField3, errorCode, crc, lenght, start;
        int timestampRaw;

        if (data != null) {
            int[] arrayInt = new int[data.length];

            for (int i = 0; i < data.length; i++) {
                arrayInt[i] = data[i] & 0xFF;
            }

            int index = 0;
            start = arrayInt[index++];
            lenght = arrayInt[index++];
            rawField1 = arrayInt[index++] | (arrayInt[index++] << 8);       // power loss
            rawField2 = arrayInt[index++] | (arrayInt[index++] << 8);       // power RX
            rawField3 = arrayInt[index++] | (arrayInt[index++] << 8);       // power TX
            timestampRaw = arrayInt[index++] | (arrayInt[index++] << 8) | (arrayInt[index++] << 16) | (arrayInt[index++] << 24);
            errorCode = arrayInt[index++] | (arrayInt[index++] << 8);
            crc = arrayInt[index++];

            powerLoss = rawField1;
            powerRX = rawField2;
            powerTX = rawField3;
            if (timestampOrigin == NO_INIT) timestampOrigin = timestampRaw;
            else timestamp = timestampRaw - timestampOrigin;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    timestamp = timestamp/100;
                    seriesPowerRX.appendData(new DataPoint(timestamp, powerRX/1000), true, 800);
                    seriesPowerTX.appendData(new DataPoint(timestamp, powerTX/1000), true, 800);
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
        stopTimer();
    }

    private void createTimer () {
        timer = new Timer();

        class periodicTimerTask extends TimerTask {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        powerRXview.setText(powerRX + " mW");
                        powerTXview.setText(powerTX + " mW");
                        powerLossView.setText(powerLoss + " mW");
                    }
                });
            }
        };

        timer.schedule(new periodicTimerTask(), 0,500);
    }

    private void stopTimer() {
        timer.cancel();
        timer.purge();
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }
}
