package com.android.BLEgiuleo;

import java.util.HashMap;



public class Enumerations {
    public static HashMap<Integer, String> ImageState = new HashMap();
    public static HashMap<Integer, String> ImageType = new HashMap();

    static {
        ImageType.put(1, "OAD_IMG_TYPE_APP");
        ImageType.put(2, "OAD_IMG_TYPE_STACK");
        ImageType.put(3, "OAD_IMG_TYPE_NP");
        ImageType.put(4, "OAD_IMG_TYPE_FACTORY");

        ImageState.put(0, "OAD_SUCCESS");
        ImageState.put(1, "OAD_CRC_ERR");
        ImageState.put(2, "OAD_FLASH_ERR");
        ImageState.put(3, "OAD_BUFFER_OFL");
    }

    public static String lookupImageType(int code) {
        String name = ImageType.get(code);
        return name;
    }

    public static String lookupImageState(int code) {
        String name = ImageState.get(code);
        return name;
    }
}
