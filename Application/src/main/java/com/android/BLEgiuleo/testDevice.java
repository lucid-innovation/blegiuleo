package com.android.BLEgiuleo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Toast;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import org.apache.commons.lang3.math.NumberUtils;
import android.media.ToneGenerator;
import android.media.AudioManager;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.*;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import android.content.BroadcastReceiver;
import com.jjoe64.graphview.DefaultLabelFormatter;

import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;

public class testDevice extends Activity {

    GraphView graph;
    EditText threshold;
    TextView packetsLost;
    TextView battery;
    TextView error;
    TextView temperature;
    TextView pressure;
    Button submit;
    double pressureVal;
    double batteryADC;
    double temperatureCelsius;
    boolean batteryFlat, sensorError, rangeTemperature;
    int packetsLostBLE = 0;
    double thresholdPressure = 100000;
    boolean toneStarted = false;

    private LineGraphSeries<DataPoint> seriesPressure = new LineGraphSeries<>();
    private int timestampRawPrev = 0;
    private static final int NO_INIT = 0xFFFFFFFF;
    private int timestampOrigin = NO_INIT;
    private double timestamp;
    private Timer timer;

    ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, ToneGenerator.MAX_VOLUME);
    DecimalFormat df = new DecimalFormat("#.00");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setTitle(R.string.title_plot);
        setContentView(R.layout.device_test);

        graph = findViewById(R.id.graph);
        submit = findViewById(R.id.submitButton);
        threshold = findViewById(R.id.thresholdData);
        packetsLost = findViewById(R.id.packetLostData);
        battery = findViewById(R.id.batteryData);
        temperature = findViewById(R.id.temperatureData);
        pressure = findViewById(R.id.pressureData);
        error = findViewById(R.id.errorData);

        // data
        graph.addSeries(seriesPressure);

        // customize a little bit viewport
        Viewport viewport = graph.getViewport();
        viewport.setYAxisBoundsManual(true);
        viewport.setXAxisBoundsManual(true);
        viewport.setMinY(-0.5);
        viewport.setMaxY(6);
        viewport.setMinX(0);
        viewport.setMaxX(40);
        graph.getGridLabelRenderer().setNumHorizontalLabels(5);
        graph.getGridLabelRenderer().setNumVerticalLabels(7);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);

        seriesPressure.setColor(Color.BLUE);
        seriesPressure.setTitle("pressure");
        graph.getLegendRenderer().setVisible(true);
        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
        // custom label formatter
        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show label x values
                    return super.formatLabel(value, isValueX) + "s";
                } else {
                    // show label for y values
                    return super.formatLabel(value, isValueX) + "KPa";
                }
            }
        });

        seriesPressure.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                String nameSeries = series.getTitle();

                if (nameSeries.compareTo("pressure") == 0) {
                    Toast.makeText(getApplicationContext(), "Sensor: " + df.format(pressureVal) + " Pa", Toast.LENGTH_SHORT).show();
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (threshold.getText().toString().isEmpty() || !NumberUtils.isCreatable(threshold.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "submit FAILED, check your data", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "submit PASSED", Toast.LENGTH_SHORT).show();
                    thresholdPressure = Double.parseDouble(threshold.getText().toString());
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        createTimer();
    }


    // Handles various events fired by the Service.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getByteArrayExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };

    private void displayData(byte[] data) {
        final double factorBattery = 0.004514;  // 4.3 Rsplitter * (4.3/4096 adc)
        final double factorPressure = 6.25;     // KPa = (digit * 256) / 40960 ==> Pa = KPa * 1000
        final int BATTERY_FLAT_MASK = 0x01;
        final int RANGE_TEMPERATURE_MASK = 0x2;
        final int SENSOR_ERROR_MASK = 0x04;
        int rawField1, rawField2, rawField3, errorCode, versionPacket;
        int timestampRaw = 0;

        if (data != null) {
            int[] arrayInt = new int[data.length];

            for (int i = 0; i < data.length; i++) {
                arrayInt[i] = data[i] & 0xFF;
            }

            int index = 0;
            versionPacket = arrayInt[index++];

            if (versionPacket == 1) {
                rawField1 = (short) (arrayInt[index++] | (arrayInt[index++] << 8));                                                     // pressure signed
                rawField2 = (byte) (arrayInt[index++]);                                                                                 // temperature signed
                timestampRaw = arrayInt[index++] | (arrayInt[index++] << 8) | (arrayInt[index++] << 16) | (arrayInt[index++] << 24);    // timestamp in ticks of 10ms
                rawField3 = arrayInt[index++] | (arrayInt[index++] << 8);                                                               // battery unsigned
                errorCode = arrayInt[index++];
                /* still 5 byte spare */
//              int spare = arrayInt[index++];
//              spare = arrayInt[index++];
//              spare = arrayInt[index++];
//              spare = arrayInt[index++];
//              spare = arrayInt[index];

                pressureVal = rawField1 / 4;
                batteryADC = rawField3 * factorBattery;
                temperatureCelsius = rawField2;
                batteryFlat = (errorCode & BATTERY_FLAT_MASK) != 0;
                rangeTemperature = (errorCode & RANGE_TEMPERATURE_MASK) != 0;
                sensorError = (errorCode & SENSOR_ERROR_MASK) != 0;
            }
            else if (versionPacket == 2) {
                rawField1 = ((arrayInt[index++] << 8) | (arrayInt[index++] << 16) | (arrayInt[index++] << 24)) >> 8;                    // pressure signed 24 bit
                rawField2 = (short)(arrayInt[index++] | (arrayInt[index++] << 8));                                                      // temperature signed
                timestampRaw = arrayInt[index++] | (arrayInt[index++] << 8) | (arrayInt[index++] << 16) | (arrayInt[index++] << 24);    // timestamp in ticks of 10ms
                rawField3 = arrayInt[index++] | (arrayInt[index++] << 8);                                                               // battery unsigned
                errorCode = arrayInt[index++];
                /* still 2 byte spare */
//              int spare = arrayInt[index++];
//              spare = arrayInt[index++];
//              spare = arrayInt[index];

                pressureVal = rawField1 * factorPressure;
                batteryADC = rawField3 * factorBattery;
                temperatureCelsius = ((double)rawField2) / 100;
                batteryFlat = (errorCode & BATTERY_FLAT_MASK) !=  0;
                rangeTemperature = (errorCode & RANGE_TEMPERATURE_MASK) !=  0;
                sensorError = (errorCode & SENSOR_ERROR_MASK) !=  0;
            }

            if (timestampOrigin == NO_INIT) timestampOrigin = timestampRaw;
            else timestamp = timestampRaw - timestampOrigin;

            // packet lost check
            int timeInterval = timestampRaw - timestampRawPrev;
            if ((timeInterval) > 10) {
                packetsLostBLE += timeInterval / 10;
                packetsLost.setText(Integer.toString(packetsLostBLE));
            }

            timestampRawPrev = timestampRaw;

//            averageData.remove(0);
//            averageData.add(49, readingADC);
//            averageADC = 0;
//            for (double item : averageData) {
//                averageADC = averageADC + item;
//            }
//            averageADC = averageADC / 50;
//            average.setText(String.format ("%.6f", averageADC));

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    seriesPressure.appendData(new DataPoint(timestamp/100, pressureVal/1000), true, 800);
                }
            });

            if (!toneStarted && (thresholdPressure < pressureVal)) {
                toneG.startTone(ToneGenerator.TONE_CDMA_DIAL_TONE_LITE);
                toneStarted = true;
            }
            else if (toneStarted && (thresholdPressure > pressureVal)) {
                toneG.stopTone();
                toneStarted = false;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
        stopTimer();
    }

    private void createTimer () {
        timer = new Timer();

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        battery.setText(df.format(batteryADC) + " V");
                        temperature.setText(df.format(temperatureCelsius) + " °C");
                        pressure.setText(df.format(pressureVal) + " Pa");

                        StringBuilder stringError = new StringBuilder(11);

                        if (batteryFlat || sensorError || rangeTemperature) {
                            if (batteryFlat) {
                                stringError.append("BAT ");
                            }
                            if (sensorError) {
                                stringError.append("SEN ");
                            }
                            if (rangeTemperature) {
                                stringError.append("TEM");
                            }

                            error.setTextColor(Color.RED);
                        }
                        else {
                            stringError.append("NONE");
                            error.setTextColor(Color.BLACK);
                        }

                        error.setText(stringError);
                    }
                });
            }
        };

        timer.schedule(timerTask, 0,1000);
    }

    private void stopTimer() {
        timer.cancel();
        timer.purge();
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }
}