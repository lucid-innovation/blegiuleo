package com.android.BLEgiuleo;

import java.util.HashMap;

/**
 * This class includes a small subset of standard GATT attributes for demonstration purposes.
 */
public class GattAttributes {
    private static HashMap<String, String> attributes = new HashMap();

    public static String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";
    public static String OAD_SERVICE = "f000ffc0-0451-4000-b000-000000000000";
    public static String OAD_CHARACTERISTIC_IDENTIFY = "f000ffc1-0451-4000-b000-000000000000";
    public static String OAD_CHARACTERISTIC_BLOCK = "f000ffc2-0451-4000-b000-000000000000";
    public static String OAD_CHARACTERISTIC_COUNT = "f000ffc3-0451-4000-b000-000000000000";
    public static String OAD_CHARACTERISTIC_STATUS = "f000ffc4-0451-4000-b000-000000000000";

    static {
        // Services.
        attributes.put("0000fff0-0000-1000-8000-00805f9b34fb", "ServiceGiuleo");
        attributes.put("0000180a-0000-1000-8000-00805f9b34fb", "Device Information Service");
        attributes.put("f000ffc0-0451-4000-b000-000000000000", "OAD Service");

        // Characteristics.
        attributes.put("00002a29-0000-1000-8000-00805f9b34fb", "Manufacturer Name String");
        attributes.put("0000fff1-0000-1000-8000-00805f9b34fb", "CharGiuleo1 RW");
        attributes.put("0000fff2-0000-1000-8000-00805f9b34fb", "CharGiuleo2 R");
        attributes.put("0000fff3-0000-1000-8000-00805f9b34fb", "Switch OAD 3 W");
        attributes.put("0000fff4-0000-1000-8000-00805f9b34fb", "Read Sensor 4 N");
        attributes.put("0000fff5-0000-1000-8000-00805f9b34fb", "CharGiuleo5 R");
        attributes.put("f000ffc1-0451-4000-b000-000000000000", "Image Identify");
        attributes.put("f000ffc2-0451-4000-b000-000000000000", "Image Block");
        attributes.put("f000ffc3-0451-4000-b000-000000000000", "Image Count");
        attributes.put("f000ffc4-0451-4000-b000-000000000000", "Image Status");

    }

    public static String lookup(String uuid, String defaultName) {
        String name = attributes.get(uuid);
        return name == null ? defaultName : name;
    }
}
